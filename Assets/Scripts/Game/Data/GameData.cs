﻿using System;

namespace TurnGame.Data
{
	[Serializable]
	public class GameData
	{
		public int currentPlayerId;
		public PlayerData player1Data;
		public PlayerData player2Data;
	}
}