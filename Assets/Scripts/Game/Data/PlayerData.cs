﻿using System;

namespace TurnGame.Data
{
	[Serializable]
	public class PlayerData
	{
		public int id;
		public UnitData[] unitsArr;
	}
}