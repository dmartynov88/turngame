﻿using System;

namespace TurnGame.Data
{
	[Serializable]
	public class UnitData
	{
		public int id;
		public int hp;
		public int baseAttack;
		public int curAttack;
		public AttackType debuffType;

		public DebuffData[] curDebuffsArr;
		public BuffData curBuff;
	}
}