﻿namespace TurnGame.Data
{
	public class AttackData
	{
		public int agressorId;
		public int victimId;
		public int agressorUnitId;
		public int victimUnitId;
		public Debuff<IDebuff> debuff;
	}
}