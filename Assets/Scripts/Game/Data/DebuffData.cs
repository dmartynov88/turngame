﻿using System;

namespace TurnGame.Data
{
	[Serializable]
	public class DebuffData
	{
		public AttackType debuffType;
		public int attackValue;
		public int stepsLeft;
	}
}