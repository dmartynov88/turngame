﻿using System;
using System.Collections.Generic;
using TurnGame.Data;

namespace TurnGame
{
	public class BuffHandler
	{
		public BuffBase BuffEffect { get; private set; }

		public BuffHandler(Unit unit, BuffData data)
		{
			SetBuff(data.buffId, unit);
		}

		public void SetBuff(BuffType id, Unit unit)
		{
			if (id != BuffType.NONE)
			{
				BuffEffect = CreateEffect(Type.GetType("TurnGame." + id), unit);
			}
		}

		private void UseBebuff()
		{
			BuffEffect.UseEffect();
		}

		private BuffBase CreateEffect(Type t, Unit unit)
		{
			return Activator.CreateInstance(t, unit) as BuffBase;
		}
	}
}