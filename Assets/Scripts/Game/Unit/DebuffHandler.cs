﻿using System;
using System.Collections.Generic;
using TurnGame.Data;

namespace TurnGame
{
	public class DebuffHandler
	{
		public event Action<Debuff<IDebuff>> OnDebuffAddedAction = (debuff) => { };
		public event Action<AttackType> OnDebuffRemovedAction = (at) => { };

		public List<Debuff<IDebuff>> EffectsList { get; protected set; }

		private Unit _unit;

		public DebuffHandler(Unit unit, DebuffData[] dataArr)
		{
			_unit = unit;
			EffectsList = new List<Debuff<IDebuff>>();

			if (dataArr != null)
			{
				foreach (var data in dataArr)
				{
					SetDebuff(CreateDebuff(data, unit));
				}
			}
		}

		public void ClearEffects()
		{
			foreach (var effect in EffectsList)
			{
				effect.Destroy();
			}
		}

		public void SetDebuff(Debuff<IDebuff> debuff)
		{
			EffectsList.Add(debuff);
			debuff.UseEffect(_unit);
			OnDebuffAddedAction(debuff);
		}

		public void RemoveDebuff(Debuff<IDebuff> debuff)
		{
			if (EffectsList.Contains(debuff))
			{
				EffectsList.Remove(debuff);
				OnDebuffRemovedAction(debuff.debuffType);
				debuff.Destroy();
			}
		}

		private Debuff<IDebuff> CreateDebuff(DebuffData data, Unit unit)
		{
			Type t = Type.GetType("TurnGame." + data.debuffType);
			IDebuff effect = Activator.CreateInstance(t, unit, data) as IDebuff;
			return new Debuff<IDebuff>(data.debuffType, effect);
		}
	}
}