﻿using System;
using TurnGame.Data;

namespace TurnGame
{
	public class Unit
	{
		public event Action OnUnitSelected = () => { };
		public event Action<int, Debuff<IDebuff>> OnAttackComplete = (victimUnitId, attack) => { };
		public event Action<int> OnHPChanged = (hp) => { };
		public event Action<int> OnAttackValueChanged = (attack) => { };

		public int UnitId { get; private set; }
		public Debuff<IDebuff> UnitAttackDebuff { get; private set; }

		public DebuffHandler DebuffHandler { get; private set; }
		public BuffHandler BuffHandler { get; private set; }

		public int BaseAttack { get; private set; }
		public int CurrentAttack 
		{ 
			get { return _currentAttack; } 
			set 
			{ 
				_currentAttack = value;
				OnAttackValueChanged(_currentAttack);
			}
		}
		private int _currentAttack;

		public Player Player { get; private set; }

		public int HP 
		{ 
			get 
			{
				return _hp;
			}
			set 
			{
				_hp = value;
				if (_hp <= 0)
				{
					_hp = 0;
					Player.DestroyUnit(this);
				}
				else
				{
					OnHPChanged(_hp);
				}
			} 
		}

		private int _hp;

		public Unit(Player player, UnitData data)
		{
			Player = player;

			UnitId = data.id;

			BaseAttack = data.baseAttack;
			CurrentAttack = data.curAttack;
			_hp = data.hp;

			Random rnd = new Random(Player.PlayerId * 5 + UnitId);
			//Set random attack type or get from saved data
			if (data.debuffType == AttackType.NONE)
			{
				var values = Enum.GetValues(typeof(AttackType));
				var attackType = (AttackType)rnd.Next(1, values.Length);
				UnitAttackDebuff = CreateUnitAttackDebuff(attackType, this);
			}
			else
			{
				UnitAttackDebuff = CreateUnitAttackDebuff(data.debuffType, this);
			}

			//Set random buff or get from saved data
			if (data.curBuff == null)
			{
				var values = Enum.GetValues(typeof(BuffType));
				var buffType = (BuffType)rnd.Next(1, values.Length);
				data.curBuff = new BuffData()
				{
					buffId = buffType
				};
			}

			DebuffHandler = new DebuffHandler(this, data.curDebuffsArr);
			BuffHandler = new BuffHandler(this, data.curBuff);
		}

		private Debuff<IDebuff> CreateUnitAttackDebuff(AttackType id, Unit unit)
		{
			Type t = Type.GetType("TurnGame." + id);
			IDebuff effect = Activator.CreateInstance(t, unit) as IDebuff;
			return new Debuff<IDebuff>(id, effect);
		}

		public void OnSelected()
		{
			OnUnitSelected();
		}

		public void AttackComplete(int victimUnitId, Debuff<IDebuff> debuff)
		{
			OnAttackComplete(victimUnitId, debuff);
		}

		public UnitData GetUnitData()
		{
			var res = new UnitData()
			{
				id = UnitId,
				hp = HP,
				curAttack = CurrentAttack,
				baseAttack = BaseAttack,
				debuffType = UnitAttackDebuff.debuffType
			};

			if (DebuffHandler.EffectsList != null)
			{
				res.curDebuffsArr = new DebuffData[DebuffHandler.EffectsList.Count];

				IDebuff debuffEffect;

				for (int d = 0; d < res.curDebuffsArr.Length; d++)
				{
					debuffEffect = DebuffHandler.EffectsList[d].debuffEffect;
					res.curDebuffsArr[d] = new DebuffData()
					{
						debuffType = debuffEffect.debuffType,
						attackValue = debuffEffect.attackValue,
						stepsLeft = debuffEffect.stepsLeft
					};
				}
			}

			return res;
		}
	}
}