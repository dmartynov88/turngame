﻿using System;
using System.Collections.Generic;
using TurnGame.Data;

namespace TurnGame
{
	public class Player
	{
		public event Action OnPlayerGetTurn = () => { };
		public event Action OnUpdateUnitsDebufs = () => { };
		public event Action<bool> OnUnitsSelectable = (selectable) => { };

		public int PlayerId { get; private set; }
		public List<Unit> UnitsList { get; private set; }

		private GameLogic _gameLogic;
		private Unit _currentUnit;

		public Player(GameLogic gameLogic, PlayerData pData)
		{
			_gameLogic = gameLogic;
			PlayerId = pData.id;

			UnitsList = new List<Unit>();
			foreach (var unitData in pData.unitsArr)
			{
				UnitsList.Add(new Unit(this, unitData));
			}
		}

		public void GetTurn()
		{
			_currentUnit = UnitsList[0];
			SelectUnit(_currentUnit);

			//Change unit turn order
			if (UnitsList.Count > 1)
			{
				UnitsList.Remove(_currentUnit);
				UnitsList.Insert(UnitsList.Count, _currentUnit);
			}

			//Fire event for simulation subscriber
			OnPlayerGetTurn();
		}

		//Make units avaliable to select
		public virtual void ActivateUnits()
		{
			OnUnitsSelectable(true);
		}

		public void SelectUnit(Unit unit)
		{
			OnUnitsSelectable(false);
			_gameLogic.UnitSelected(this, unit);
			unit.OnSelected();
		}

		//Unit attack complete
		public void AttackCompleteHandler(int unitId, int victimUnitId, Debuff<IDebuff> debuff)
		{
			var unit = GetUnitById(unitId);
			if (unit != null)
			{
				unit.AttackComplete(victimUnitId, debuff);
			}
		}

		//Receive unit damage 
		public void ReceiveDamageHandler(int unitId, Debuff<IDebuff> debuff)
		{
			var unit = GetUnitById(unitId);
			if (unit != null)
			{
				unit.DebuffHandler.SetDebuff(debuff);
			}
		}

		public virtual void DestroyUnit(Unit unit)
		{
			UnitsList.Remove(unit);
			unit.DebuffHandler.ClearEffects();
		}

		public void UpdateUnitsDebufs()
		{
			OnUpdateUnitsDebufs();
		}

		private Unit GetUnitById(int unitId)
		{
			return UnitsList.Find(u => u.UnitId == unitId);
		}

		internal PlayerData CreatePlayerData()
		{
			PlayerData pData = new PlayerData();
			pData.id = PlayerId;
			pData.unitsArr = new UnitData[UnitsList.Count];

			for (int i = 0; i < UnitsList.Count; i++)
			{
				pData.unitsArr[i] = UnitsList[i].GetUnitData();
			}

			return pData;
		}
}
}