﻿using TurnGame.Data;

namespace TurnGame
{
	public class AttackHandler
	{
		private AttackData _attackData;

		public void CreateAttackData(int agressorId, int victimId)
		{
			_attackData = new AttackData()
			{
				agressorId = agressorId,
				victimId = victimId
			};
		}

		public void SelectAttackUnit(Unit agressorUnit)
		{
			_attackData.agressorUnitId = agressorUnit.UnitId;
			_attackData.debuff = agressorUnit.UnitAttackDebuff;
		}

		public void SelectVictimUnit(Unit victimUnit)
		{
			_attackData.victimUnitId = victimUnit.UnitId;
		}

		public AttackData GetAttackData()
		{
			return _attackData;
		}
	}
}