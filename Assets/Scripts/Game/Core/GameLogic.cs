﻿using System;
using System.Collections.Generic;
using TurnGame.Data;

namespace TurnGame
{
	public class GameLogic
	{
		//events for simulation
		public event Action<AttackData> OnReadyToAttack = (aData) => { };
		public event Action<int> OnGameComplete = (pId) => { };

		public Player CurrentPlayer { get; private set; }

		private Player[] _players;
		private AttackHandler _attackHandler;


		public GameLogic()
		{
			_players = new Player[2];
			_attackHandler = new AttackHandler();
		}

		#region Turn logic

		public void StartGame(int currentPlayerId, Player player1, Player player2)
		{
			_players[0] = player1;
			_players[1] = player2;
			CurrentPlayer = GetPlayerById(currentPlayerId);

			CheckGameComplete();
		}

		private void CheckGameComplete()
		{
			if (_players[0].UnitsList.Count == 0)
			{
				GameComplete(_players[1]);
			}
			else if (_players[1].UnitsList.Count == 0)
			{
				GameComplete(_players[0]);
			}
			else
			{
				Turn();
			}
		}

		private void Turn()
		{
			if (CurrentPlayer.PlayerId == _players[0].PlayerId)
			{
				CurrentPlayer = _players[1];
				_attackHandler.CreateAttackData(CurrentPlayer.PlayerId, _players[0].PlayerId);
			}
			else
			{
				CurrentPlayer = _players[0];
				_attackHandler.CreateAttackData(CurrentPlayer.PlayerId, _players[1].PlayerId);
			}

			CurrentPlayer.GetTurn();
		}

		private void GameComplete(Player winner)
		{
			OnGameComplete(CurrentPlayer.PlayerId);
		}

		#endregion

		#region unit selection

		/// <summary>
		/// Sort selected unit as Agressor or Victim.
		/// </summary>
		/// <param name="player">Player unit owner</param>
		/// <param name="selectedUnit">Selected unit.</param>
		public void UnitSelected(Player player, Unit selectedUnit)
		{
			if (player == CurrentPlayer)
			{
				SelectAgressorUnit(selectedUnit);
				if (player == _players[0])
					_players[1].ActivateUnits();
				else
					_players[0].ActivateUnits();
			}
			else
			{
				SelectVictimUnit(selectedUnit);
			}
		}

		private void SelectAgressorUnit(Unit agressorUnit)
		{
			_attackHandler.SelectAttackUnit(agressorUnit);
		}

		private void SelectVictimUnit(Unit victimUnit)
		{
			_attackHandler.SelectVictimUnit(victimUnit);

			//Fire event player ready to attack
			OnReadyToAttack(_attackHandler.GetAttackData());
		}
		#endregion

		//Process attack data from simulation and returns attack actions list for simulation
		public List<Action> ProcessAttackData(AttackData attackData)
		{
			List<Action> res = new List<Action>();
			if (attackData.victimId == _players[0].PlayerId)
			{
				res.Add(() =>
				{
					_players[1].AttackCompleteHandler(attackData.agressorUnitId, attackData.victimUnitId, attackData.debuff);
				});
				res.Add(() =>
				{
					_players[0].ReceiveDamageHandler(attackData.victimUnitId, attackData.debuff);
				});
			}
			else
			{
				res.Add(() =>
				{
					_players[0].AttackCompleteHandler(attackData.agressorUnitId, attackData.victimUnitId, attackData.debuff);
				});
				res.Add(() =>
				{
					_players[1].ReceiveDamageHandler(attackData.victimUnitId, attackData.debuff);
				});

			}

			res.Add(() => 
			{
				if (_players[0].UnitsList.Count != 0 && _players[1].UnitsList.Count != 0)
				{
					_players[0].UpdateUnitsDebufs();
					_players[1].UpdateUnitsDebufs();
				}
			});

			res.Add(() =>
			{
				CheckGameComplete();
			});
			return res;
		}

		public GameData GetGameProgressData()
		{
			if (_players[0] == null)
				return null;
			
			GameData gd = new GameData();

			gd.currentPlayerId = CurrentPlayer.PlayerId;

			gd.player1Data = _players[0].CreatePlayerData();
			gd.player2Data = _players[1].CreatePlayerData();

			return gd;
		}

		private Player GetPlayerById(int playerId)
		{
			if (_players[0].PlayerId == playerId)
			{
				return _players[0];
			}
			else
			{
				return _players[1];
			}
		}
	}
}