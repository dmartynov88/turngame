﻿using System;

namespace TurnGame
{
	public class UnitEffect
	{
		public event Action OnEffectDestroyed = () => { };

		public virtual void UseEffect()
		{

		}

		public void Destroy()
		{
			OnEffectDestroyed();
		}
	}
}