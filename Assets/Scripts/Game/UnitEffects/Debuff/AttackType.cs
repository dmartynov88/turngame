﻿namespace TurnGame
{
	public enum AttackType
	{
		NONE = 0,
		RegularAttack,
		Bleed,
		HalfHP
	}
}