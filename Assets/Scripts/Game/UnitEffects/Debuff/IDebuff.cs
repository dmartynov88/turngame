﻿using TurnGame;

public interface IDebuff
{
	AttackType debuffType { get; }
	int attackValue { get; }
	int stepsLeft { get; }
	void UseEffect(Unit enemyUnit, Debuff<IDebuff> debuff);
	void DestroyEffect();
}
