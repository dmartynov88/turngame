﻿
using System;
using System.Runtime.Serialization;

namespace TurnGame
{
	[Serializable]
	public class Debuff<T> where T : IDebuff
	{
		public event Action OnEffectDestroyed = () => { };
		public AttackType debuffType;

		public T debuffEffect;


		public Debuff(AttackType debuffType, T debuffEffect)
		{
			this.debuffType = debuffType;
			this.debuffEffect = debuffEffect;
		}

		public void UseEffect(Unit enemyUnit)
		{
			debuffEffect.UseEffect(enemyUnit, this as Debuff<IDebuff>);
		}

		public void Destroy()
		{
			debuffEffect.DestroyEffect();
			OnEffectDestroyed();
		}
	}
}
