﻿using System;
using System.Text;
using TurnGame.Data;

namespace TurnGame
{
	public class Bleed : IDebuff
	{
		public AttackType debuffType { get { return AttackType.Bleed; } }
		public int attackValue { get; private set; }
		public int stepsLeft { get; private set; }

		private Unit _owner;
		private Unit _enemyUnit;

		public Bleed(Unit unit, DebuffData data)
		{
			_owner = unit;
			attackValue = data.attackValue;
			stepsLeft = data.stepsLeft;
		}

		public Bleed(Unit unit)
		{
			_owner = unit;
			attackValue = _owner.BaseAttack / 2;
			stepsLeft = 1;
		}

		public void UseEffect(Unit enemyUnit, Debuff<IDebuff> debuff)
		{
			_enemyUnit = enemyUnit;
			_enemyUnit.Player.OnUpdateUnitsDebufs += OnUpdate;
		}

		void OnUpdate()
		{
			if (_enemyUnit != null)
			{
				_enemyUnit.HP -= attackValue;
			}
		}

		public void DestroyEffect()
		{
			if (_enemyUnit != null)
			{
				_enemyUnit.Player.OnUpdateUnitsDebufs -= OnUpdate;
				_enemyUnit = null;
			}
		}
	}
}