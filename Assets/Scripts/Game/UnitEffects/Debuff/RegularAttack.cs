﻿using System;
using System.Text;
using TurnGame.Data;

namespace TurnGame
{
	[Serializable]
	public class RegularAttack : IDebuff
	{
		
		private Unit _owner;

		public AttackType debuffType { get { return AttackType.RegularAttack; } }

		public int attackValue { get; private set; }

		public int stepsLeft { get; private set; }

		//read from saves
		public RegularAttack(Unit unit, DebuffData data)
		{
			_owner = unit;
			attackValue = data.attackValue;
			stepsLeft = data.stepsLeft;
		}

		//Set data
		public RegularAttack(Unit unit)
		{
			_owner = unit;
			attackValue = unit.CurrentAttack;
			stepsLeft = 1;
		}

		public void UseEffect(Unit enemyUnit, Debuff<IDebuff> debuff)
		{
			stepsLeft--;
			enemyUnit.HP -= _owner.BaseAttack;
			enemyUnit.DebuffHandler.RemoveDebuff(debuff);
		}

		public void DestroyEffect() { }
	}
}