﻿using System;
using System.Text;
using TurnGame.Data;

namespace TurnGame
{
	public class HalfHP : IDebuff, ILockAttack
	{
		public AttackType debuffType { get { return AttackType.HalfHP; } }
		public int attackValue { get; private set; }
		public int stepsLeft { get; private set; }

		private Unit _owner;
		private Unit _enemyUnit;
		private Debuff<IDebuff> _debuffEffect;

		public HalfHP(Unit unit, DebuffData data)
		{
			_owner = unit;
			attackValue = data.attackValue;
			stepsLeft = data.stepsLeft;
		}

		public HalfHP(Unit unit)
		{
			_owner = unit;
			attackValue = _owner.BaseAttack / 2;
			stepsLeft = 3;
		}

		public void UseEffect(Unit enemyUnit, Debuff<IDebuff> debuff)
		{
			_enemyUnit = enemyUnit;
			_debuffEffect = debuff;
			_enemyUnit.Player.OnUpdateUnitsDebufs += OnUpdate;
			_enemyUnit.CurrentAttack = _enemyUnit.BaseAttack / 2;
			_enemyUnit.HP -= attackValue;
		}

		void OnUpdate()
		{
			stepsLeft--;
			if (_enemyUnit != null)
			{
				if (stepsLeft <= 0)
				{
					if (_enemyUnit.DebuffHandler.EffectsList.Find(e => e.debuffEffect is ILockAttack && e != _debuffEffect) == null)
					{
						_enemyUnit.CurrentAttack = _enemyUnit.BaseAttack;
					}
					_enemyUnit.DebuffHandler.RemoveDebuff(_debuffEffect);
				}
			}
		}

		public void DestroyEffect()
		{
			_enemyUnit.Player.OnUpdateUnitsDebufs -= OnUpdate;
			_enemyUnit = null;
		}
	}
}