﻿
namespace TurnGame
{
	public abstract class BuffBase : UnitEffect
	{
		public BuffBase(Unit unit) 
		{

		}

		public abstract BuffType GetBuffType();
	}
}
