﻿namespace TurnGame
{
	public class ForceAttack : BuffBase
	{
		public ForceAttack(Unit unit) : base(unit)
		{
			//Subscribe to change HP
		}

		public override BuffType GetBuffType()
		{
			return BuffType.ForceAttack;
		}

		public override void UseEffect()
		{
			//force current attack value = base attack * X
		}
	}
}