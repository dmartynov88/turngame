﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public enum Windows
{
	MAIN,
	GAME,
	PAUSE
}

public class UIManager : MonoBehaviour
{
	private Dictionary<Windows, UIWindowBase> _uiWindows = new Dictionary<Windows, UIWindowBase>();

	public void Init(Core core)
	{
		_uiWindows.Clear();
		_uiWindows.Add(Windows.MAIN, GetComponentInChildren<MainView>());
		_uiWindows.Add(Windows.GAME, GetComponentInChildren<GameView>());
		_uiWindows.Add(Windows.PAUSE, GetComponentInChildren<PauseView>());

		foreach (var view in _uiWindows.Values)
		{
			view.Init(core, core.GameController);
		}

		ShowWindow(Windows.MAIN);
	}

	public T GetWindow<T>(Windows windowId) where T : UIWindowBase
	{
		return _uiWindows[windowId] as T;
	}

	public void ShowWindow(Windows windowId)
	{
		_uiWindows[windowId].Show();
	}

	
	public void HideWindow(Windows windowId)
	{
		_uiWindows[windowId].Hide();
	}
}
