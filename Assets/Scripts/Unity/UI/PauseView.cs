﻿using UnityEngine;
using UnityEngine.UI;

public class PauseView : UIWindowBase
{
	[SerializeField]
	private Button _playBtn;

	public override void Show()
	{
		base.Show();
		_playBtn.interactable = !_gameController.isGameComplete;
	}


	public void OnPlayBtn()
	{
		_core.PauseGame(false);
	}

	public void OnRestartBnt()
	{
		_gameController.Reset();
		_gameController.OnStartGame(true);
		_core.PauseGame(false);
	}
}
