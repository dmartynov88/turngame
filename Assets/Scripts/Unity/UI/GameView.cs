﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameView : UIWindowBase
{

	[SerializeField]
	private UnitView[] enemyUnits;
	[SerializeField]
	private UnitView[] playerUnits;

	public override void Show()
	{
		base.Show();
		_gameController.OnUpdateUnitsList += OnUpdateUnitsList;

		for (int i = 0; i < 5; i++)
		{
			enemyUnits[i].Destroy();
			if (_gameController.Bot.GetUnitById(i) != null)
			{
				enemyUnits[i].gameObject.SetActive(true);
				enemyUnits[i].Init(_gameController.Bot.GetUnitById(i).Unit);
			}
			else
			{
				enemyUnits[i].gameObject.SetActive(false);
			}


			playerUnits[i].Destroy();
			if (_gameController.Player.GetUnitById(i) != null)
			{
				playerUnits[i].gameObject.SetActive(true);
				playerUnits[i].Init(_gameController.Player.GetUnitById(i).Unit);
			}
			else
			{
				playerUnits[i].gameObject.SetActive(false);
			}
		}

		OnUpdateUnitsList();
	}


	void OnUpdateUnitsList()
	{
		for (int i = 0; i < 5; i++)
		{
			if (_gameController.Bot.GetUnitById(i) == null)
			{
				enemyUnits[i].gameObject.SetActive(false);
			}

			if (_gameController.Player.GetUnitById(i) == null)
			{
				playerUnits[i].gameObject.SetActive(false);
			}
		}
	}

	public void OnPauseBtn()
	{
		_core.PauseGame(true);
	}

	public void OnTurnBtn()
	{
		_gameController.OnProcessPlayerAttack();
	}

	public override void Hide()
	{
		base.Hide();
		if (_gameController.Player != null)
		{
			_gameController.OnUpdateUnitsList -= OnUpdateUnitsList;
		}
	}
}
