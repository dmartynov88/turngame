﻿using System.Collections.Generic;
using TurnGame;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitView : MonoBehaviour
{
	[SerializeField]
	private Image unitAttackIcon;
	[SerializeField]
	private TMP_Text hpLabel;
	[SerializeField]
	private TMP_Text atackValueLabel;
	[SerializeField]
	private Image selectionImage;
	[SerializeField]
	private Button unitBtn;
	[SerializeField]
	private List<Image> debuffImages;


	private Unit _unit;
	private Dictionary<AttackType, Image> _unitDebuffsDict = new Dictionary<AttackType, Image>();




	public void Init(Unit unit)
	{
		_unit = unit;
		_unit.OnUnitSelected += OnUnitSelected;

		_unit.OnHPChanged += OnHPChanged;
		_unit.OnAttackValueChanged += OnAttackValueChanged;

		_unit.DebuffHandler.OnDebuffAddedAction += OnDebuffAddedAction;
		_unit.DebuffHandler.OnDebuffRemovedAction += OnDebuffRemovedAction;

		_unit.Player.OnUnitsSelectable += OnUnitsSelectable;

		foreach (var icon in debuffImages)
		{
			icon.enabled = false;
		}

		if (_unit.UnitAttackDebuff.debuffType != AttackType.RegularAttack)
		{
			unitAttackIcon.enabled = true;
			SetDebuffIcon(unitAttackIcon, _unit.UnitAttackDebuff.debuffType);
		}
		else
		{
			unitAttackIcon.enabled = false;
		}

		OnHPChanged(_unit.HP);
		OnAttackValueChanged(_unit.CurrentAttack);

		foreach (var debuff in _unit.DebuffHandler.EffectsList)
		{
			if (debuff.debuffType != AttackType.RegularAttack && !_unitDebuffsDict.ContainsKey(debuff.debuffType))
			{
				var img = debuffImages.Find(i => !i.enabled);
				_unitDebuffsDict.Add(debuff.debuffType, img);
				SetDebuffIcon(img, debuff.debuffType);
			}
		}
		selectionImage.enabled = false;
		OnUnitsSelectable(false);
	}

	public void OnUnitBtnClick()
	{
		_unit.Player.SelectUnit(_unit);
	}

	void OnUnitsSelectable(bool isSelectable)
	{
		if (unitBtn != null)
		{
			unitBtn.interactable = isSelectable;
		}
		UnitDeselect();
	}

	void SetDebuffIcon(Image img, AttackType type)
	{
		img.sprite = Resources.Load<Sprite>("Icons/" + type + "_icon");
		img.enabled = true;
	}

	void OnHPChanged(int hp)
	{
		hpLabel.text = "HP: " + hp;
	}

	void OnAttackValueChanged(int attack)
	{
		atackValueLabel.text = "DMG: " + attack;
	}

	void OnUnitSelected()
	{
		selectionImage.enabled = true;
	}

	void UnitDeselect()
	{
		selectionImage.enabled = false;
	}

	void OnDebuffAddedAction(Debuff<IDebuff> debuff)
	{
		if (debuff.debuffType != AttackType.RegularAttack && !_unitDebuffsDict.ContainsKey(debuff.debuffType))
		{
			var img = debuffImages.Find(i => !i.enabled);
			_unitDebuffsDict.Add(debuff.debuffType, img);
			SetDebuffIcon(img, debuff.debuffType);
		}
	}

	void OnDebuffRemovedAction(AttackType debuffType)
	{
		if (debuffType != AttackType.RegularAttack && _unit.DebuffHandler.EffectsList.Find(d => d.debuffType == debuffType) == null)
		{
			var img = _unitDebuffsDict[debuffType];
			_unitDebuffsDict.Remove(debuffType);
			if (img != null)
			{
				img.enabled = false;
			}
		}
	}

	public void Destroy()
	{
		if (_unit != null)
		{
			_unit.OnUnitSelected -= OnUnitSelected;

			_unit.OnHPChanged -= OnHPChanged;
			_unit.OnAttackValueChanged -= OnAttackValueChanged;

			_unit.DebuffHandler.OnDebuffAddedAction -= OnDebuffAddedAction;
			_unit.DebuffHandler.OnDebuffRemovedAction -= OnDebuffRemovedAction;

			_unit = null;
		}
	}
}
