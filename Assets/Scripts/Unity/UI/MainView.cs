﻿using UnityEngine;
using UnityEngine.UI;

public class MainView : UIWindowBase
{
	public void OnPlayBtn()
	{
		_core.UI.HideWindow(Windows.MAIN);
		_gameController.OnStartGame(false);
		_core.PauseGame(false);
	}
}
