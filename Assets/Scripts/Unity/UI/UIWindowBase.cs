﻿using UnityEngine;

public class UIWindowBase : MonoBehaviour
{
	[SerializeField]
	private CanvasGroup _group;

	protected Core _core;
	protected UnityGameController _gameController;

	public void Init(Core core, UnityGameController gameController)
	{
		_core = core;
		_gameController = gameController;
		Hide();
	}

	public virtual void Show()
	{
		_group.alpha = 1;
		_group.blocksRaycasts = true;
	}
	public virtual void Hide()
	{
		_group.alpha = 0;
		_group.blocksRaycasts = false;
	}
}
