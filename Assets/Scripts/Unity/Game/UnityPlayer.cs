﻿using System;
using System.Collections.Generic;
using TurnGame;
using TurnGame.Data;
using UnityEngine;

public class UnityPlayer : Player
{
	private List<UnityUnit> _unityUnitsList;
	private UnityGameController _ugc;
	private Transform _parent;

	public UnityPlayer(GameLogic gameLogic, PlayerData pData, UnityGameController ugc, Transform parent) : base(gameLogic, pData)
	{
		_ugc = ugc;
		_parent = parent;
		_unityUnitsList = new List<UnityUnit>();

		foreach (var unit in UnitsList)
		{
			InstantiateUnit(unit);
		}
	}

	public UnityUnit GetUnitById(int unitId)
	{
		return _unityUnitsList.Find(u => u.UnitId == unitId);
	}

	public override void DestroyUnit(Unit unit)
	{
		base.DestroyUnit(unit);

		UnityUnit unitController = GetUnitById(unit.UnitId);
		if (unitController != null)
		{
			_unityUnitsList.Remove(unitController);
			unitController.Destroy(true);
		}
	}

	public void Destroy()
	{
		foreach (var unit in _unityUnitsList)
		{
			unit.Destroy();
		}
	}

	private void InstantiateUnit(Unit unit)
	{
		var unitGO = GameObject.Instantiate(Resources.Load("Unit" + PlayerId) as GameObject);
		var unitController = unitGO.GetComponent<UnityUnit>();

		_unityUnitsList.Add(unitController);
		unitController.Init(_ugc, unit);

		unitGO.transform.parent = _parent;
		unitGO.transform.localPosition = new Vector3(unit.UnitId * 2, 0f, 0f);
	}
}