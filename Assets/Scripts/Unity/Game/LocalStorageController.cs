﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TurnGame.Data;
using UnityEngine;

public class LocalStorageController: IStorageController
{
	public int LocalPlayerId { get { return playerId; } }
	private int playerId = 1; //TODO Stub

	private readonly string path;

	public LocalStorageController()
	{
		path = Application.persistentDataPath + "savedGameData.json";
	}

	public GameData LoadGameProgress(bool startNewGame)
	{
		if (!startNewGame && File.Exists(path))
		{
			string json = File.ReadAllText(path);
			if (!string.IsNullOrEmpty(json))
			{
				return JsonUtility.FromJson<GameData>(json);
			}
		}
		return GenerateGameData();
	}

	public void SaveGameProgress(GameData gameData)
	{
		if (gameData != null)
		{
			string json = JsonUtility.ToJson(gameData);
			File.WriteAllText(path, json);
		}
	}

	//Test local data generation stub
	private GameData GenerateGameData()
	{
		GameData res = new GameData();

		res.currentPlayerId = playerId;

		res.player1Data = new PlayerData()
		{
			id = 1,
			unitsArr = new UnitData[5]
		};

		res.player2Data = new PlayerData()
		{
			id = 2,
			unitsArr = new UnitData[5]
		};

		for (int i = 0; i < 5; i++)
		{
			res.player1Data.unitsArr[i] = new UnitData()
			{
				id = i,
				hp = 10,
				baseAttack = 5,
				curAttack = 5,
				debuffType = TurnGame.AttackType.NONE
			};

			res.player2Data.unitsArr[i] = new UnitData()
			{
				id = i,
				hp = 10,
				baseAttack = 5,
				curAttack = 5,
				debuffType = TurnGame.AttackType.NONE
			};
		}

		SaveGameProgress(res);

		return res;
	}
}
