﻿using System;
using System.Collections;
using TurnGame;
using TurnGame.Data;
using UnityEngine;

public class UnityBot
{
	public int PlayerId { get { return BotUnityPlayer.PlayerId; } }

	public UnityPlayer BotUnityPlayer { get; private set; }

	private UnityGameController _ugc;
	private GameLogic _gl;

	private UnityPlayer _player;


	public UnityBot(GameLogic gl, PlayerData pData, UnityGameController ugc, Transform parent, UnityPlayer player)
	{
		_ugc = ugc;

		_gl = gl;
		_gl.OnReadyToAttack += OnReadyToAttack;

		_player = player;
		BotUnityPlayer = new UnityPlayer(_gl, pData, _ugc, parent);
		BotUnityPlayer.OnPlayerGetTurn += SelectEnemyAndAttack;
	}

	public UnityUnit GetUnitById(int unitId)
	{
		return BotUnityPlayer.GetUnitById(unitId);
	}

	void SelectEnemyAndAttack()
	{
		_ugc.StartCoroutine(Wait(() =>
		{
			_player.SelectUnit(_player.UnitsList[UnityEngine.Random.Range(0, _player.UnitsList.Count)]);
		}));
	}

	IEnumerator Wait(Action onComplete)
	{
		yield return new WaitForSeconds(0.5f);
		onComplete();
	}

	void OnReadyToAttack(AttackData aData)
	{
		if (aData.agressorId == BotUnityPlayer.PlayerId)
			_ugc.StartCoroutine(Wait(() =>
		{
			_ugc.ProcessAttack();
		}));
	}

	public void Destroy()
	{
		BotUnityPlayer.OnPlayerGetTurn -= SelectEnemyAndAttack;
		BotUnityPlayer.Destroy();
	}
}