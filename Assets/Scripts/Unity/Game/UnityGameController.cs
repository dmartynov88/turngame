﻿using System;
using System.Collections;
using System.Collections.Generic;
using TurnGame;
using TurnGame.Data;
using UnityEngine;

public class UnityGameController : MonoBehaviour 
{
	public event Action OnUpdateUnitsList = () => { };

	public bool isGameComplete { get; private set; }

	public UnityPlayer Player { get; private set; }
	public UnityBot Bot { get; private set; }
	public GameLogic GameLogic { get; private set; }

	[SerializeField]
	Transform _player1Parent;
	[SerializeField]
	Transform _player2Parent;


	private AttackData attackData;
	private bool isReadyToAttack;
	private LocalStorageController _localStorage;

	public void OnStartGame(bool startNewGame)
	{
		_localStorage = new LocalStorageController();
		GameData gd = _localStorage.LoadGameProgress(startNewGame);
		isGameComplete = false;

		GameLogic = new GameLogic();
		GameLogic.OnReadyToAttack += OnReadyToAttack;
		GameLogic.OnGameComplete += OnGameComplete;

		Player = new UnityPlayer(GameLogic, gd.player1Data, this, _player1Parent);
		Bot = new UnityBot(GameLogic, gd.player2Data, this, _player2Parent, Player);

		GameLogic.StartGame(gd.currentPlayerId, Player, Bot.BotUnityPlayer);
	}

	public void Reset()
	{
		isReadyToAttack = false;
		Player.Destroy();
		Bot.Destroy();
		if (GameLogic != null)
		{
			GameLogic.OnReadyToAttack -= OnReadyToAttack;
			GameLogic.OnGameComplete -= OnGameComplete;
			GameLogic = null;
			_localStorage = null;
		}
	}

	void OnGameComplete(int playerId)
	{
		isGameComplete = true;

		Reset();

		Core.Instance.PauseGame(true);
	}

	public UnityUnit GetEnemyUnitById(int victimUnitId)
	{
		if (GameLogic.CurrentPlayer.PlayerId == Player.PlayerId)
		{
			return Bot.GetUnitById(victimUnitId);
		}
		else
		{
			return Player.GetUnitById(victimUnitId);
		}
	}

	void OnReadyToAttack(AttackData attackData)
	{
		this.attackData = attackData;
		isReadyToAttack = true;
	}

	//Call on Attack btn click
	public void OnProcessPlayerAttack()
	{
		if (GameLogic.CurrentPlayer == Player && isReadyToAttack)
		{
			isReadyToAttack = false;
			ProcessAttack();
		}
	}

	//Call from bot class
	public void ProcessAttack()
	{
		StartCoroutine(InvokeAttackActions(GameLogic.ProcessAttackData(attackData)));
	}

	IEnumerator InvokeAttackActions(List<Action> actions)
	{
		foreach (var act in actions)
		{
			act.Invoke();
			OnUpdateUnitsList();
			yield return new WaitForSeconds(0.25f);
		}
	}

	void OnApplicationQuit()
	{
		if (_localStorage != null && GameLogic != null)
		{
			_localStorage.SaveGameProgress(GameLogic.GetGameProgressData());
		}
	}
}