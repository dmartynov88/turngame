﻿using System;
using TurnGame.Data;

public interface IStorageController
{
	int LocalPlayerId { get; }

	GameData LoadGameProgress(bool startNewGame);
	void SaveGameProgress(GameData gameData);
}
