﻿using System;
using TurnGame;
using System.Collections;
using UnityEngine;

public class UnityUnit : MonoBehaviour
{
	public int UnitId { get { return Unit.UnitId; } }
	public Unit Unit { get; private set; }

	private UnityGameController _ugc;

	public void Init(UnityGameController ugc, Unit unit)
	{
		_ugc = ugc;

		Unit = unit;
		Unit.OnUnitSelected += OnUnitSelected;
		Unit.OnAttackComplete += OnAttackComplete;
		Unit.OnHPChanged += OnHPChanged;
		Unit.DebuffHandler.OnDebuffAddedAction += OnDebuffAdded;
	}

	void OnUnitSelected()
	{
		
	}

	void OnAttackComplete(int victimUnitId, Debuff<IDebuff> debuff)
	{
		gameObject.GetComponent<Animator>().SetTrigger("Attack");

		UnityUnit victim = _ugc.GetEnemyUnitById(victimUnitId);

		if (victim != null)
		{
			transform.LookAt(victim.transform);
		}
	}

	void OnDebuffAdded(Debuff<IDebuff> debuf)
	{
		
	}

	void OnHPChanged(int hp)
	{
		
	}

	public void Destroy(bool playDeathAnimation = false)
	{
		if (Unit != null)
		{
			Unit.OnUnitSelected -= OnUnitSelected;
			Unit.OnAttackComplete -= OnAttackComplete;
			Unit.OnHPChanged -= OnHPChanged;
			Unit.DebuffHandler.OnDebuffAddedAction -= OnDebuffAdded;
			Unit = null;
			if (playDeathAnimation)
			{
				GameObject.Destroy(gameObject, 1.5f);
				gameObject.GetComponent<Animator>().SetTrigger("Die");
			}
			else
			{
				GameObject.Destroy(gameObject);
			}
		}
	}
}
