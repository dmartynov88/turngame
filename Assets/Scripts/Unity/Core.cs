﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{
	public static Core Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<Core>();
			}
			return _instance;
		}
	}
	private static Core _instance;

	public UIManager UI
	{
		get
		{
			return _uiManager;
		}
	}
	[SerializeField]
	private UIManager _uiManager;

	public UnityGameController GameController
	{
		get
		{
			return _game;
		}
	}
	[SerializeField]
	private UnityGameController _game;


	void Awake()
	{
		_instance = this;
		_uiManager.Init(_instance);
	}

	public void PauseGame(bool pause)
	{
		if (pause)
		{
			_uiManager.HideWindow(Windows.GAME);
			_uiManager.ShowWindow(Windows.PAUSE);
			Time.timeScale = 0f;
		}
		else
		{
			_uiManager.HideWindow(Windows.PAUSE);
			_uiManager.ShowWindow(Windows.GAME);
			Time.timeScale = 1f;
		}
	}
}
